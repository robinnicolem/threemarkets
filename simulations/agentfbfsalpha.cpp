

/*!
 @file agentfbfsalpha.cpp
 * @author Robin NICOLE
 * @version 1.0
 * @brief agents with fixed preferences to buy and sell, they only update their preferences toward market 1 and 2
 */

#ifndef AGENTFBFSALPHA_CPP
#include "agentfbfsalpha.h"
#define AGENTFBFSALPHA_CPP
  /*! 
    !@brief reset the parameters of the agents for a new turn
    !*/ 
void agentfbfsalpha::ComputeTimeSerie(string name)
  {  
    TimeSerieOutput.open(name);
    TimeSerieOutput <<"alpha" <<"\t"
		    <<"fparam" <<"\t"
		    <<"id" <<"\t"
		    <<"proba" <<"\t"
		    <<"Temp" <<"\t"
		    <<"A1" <<"\t"
		    <<"A2" <<endl;
}
  
void agentfbfsalpha::newturn(){
  bid = 0;
}
/*! 
 * @brief initialisation of the agent
 * @param sig : variance of the agent
 * @param forget : forgetting parameter of the agent
 * @param mb : average bid of the agent
 * @param ms : average seling price of the agent
 * @param idee : a number to identify the agent
 * @param Temp : temperatue of the agent 
 * @param probab : pointer to a double which is the probability of buying of the agent
 !*/ 
void agentfbfsalpha::init(const double& sig,const double& forget,const double& mb,const double& ms,const int& idee,const double& T,void* probab){
  sigma = sig;
  fparam = forget ;
  mean_buy = mb;
  mean_sell  = ms;
  id = idee ;
  choice = 1;
  action = 0;
  success=false;
  proba=*((double*)probab);
  //   cout << id<< endl ;
  last_m=1;
  Temp = T;
  last_score = 0 ;
  A1 = 0 ;
  A2 = 0 ;
  A3 = 0 ; 
  //delta = 0  ;
  cgscore = 0 ; 
}
int agentfbfsalpha::init_score(const double& var)
{
  A1 = 0;//rand_gaussian(var,0);
  A2 = 0;//rand_gaussian(var,0);
  A3 = 0;//rand_gaussian(var,0);
  return 0;
}
int agentfbfsalpha::choice_market(){
  
  double random = myran.doub();
  vector<double> result(CumulativeProba(Temp));
  if (random < result[0])
    {
      choice = 1;
    }
  else if(random < result[1])
    {
      choice = 2 ;
    }
  else
    {
      choice = 3;
    }
  action = choose_action();
  return choice;
}
/*! 
  ! @brief get a random number between 0 and 1 and if it is lower than the proba of buying then buy else sell
  !*/ 
int agentfbfsalpha::choose_action(){
  double rand = myran.doub();
  if( rand < proba)
    {
      action = 0;
      return 0;
    }
  else 
    {
      action = 1;
     return 1;
    }
}

/*! 
 * @brief sigmoid(delta) is the probability to buy at market 1
  !*/ 
vector<double> agentfbfsalpha::CumulativeProba(const double& Temp) const {
  double proba1(exp(A1/Temp));
  double proba2(exp(A2/Temp));
  double proba3(exp(A3/Temp));
  double Z = proba1 + proba2 + proba3;
  vector<double> result({proba1/Z, (proba1+proba2)/Z});
  return result;
}
 /*!
  * @brief return the agent offer 
  */ 
double agentfbfsalpha::offer(){
  double output(0) ;
  if (action==0)  output = rand_gaussian(1.,mean_buy);
  if (action==1)  output = rand_gaussian(1.,mean_sell);
  bid = output;
  return output;
}

double agentfbfsalpha::rand_gaussian(const double& stdev,const double& mean) const {
  double v1,v2,s;
  do {
    v1=2.0*myran.doub()-1.;
    v2=2.0*myran.doub()-1.;
    s = v1*v1 + v2*v2;
  } while ( s >= 1.0 );

  if (s == 0.0)
    return 0.0;
  else
    return (mean+stdev*v1*sqrt(-2.0 * log(s) / s));
}
double agentfbfsalpha::get_doub(const string& name) // const override
{
  if (name == "T") return Temp ; 
  else if (name == "cgscore") return cgscore ;
  else 
    {
      cerr << "Wrong name of variable to get" << endl ;
      return 0.; 
    }
}

int agentfbfsalpha::change_parameter(const string& name,const double& val){
  

  if (name == "alpha" )   // Changing variable alpha
    {
      if (val > 1. || val < 0.) 
	{
	  cerr << "the value of alpha must be between 0 and 1" << endl ;
	  return 1;
	}
      alpha = val;
      return 0;
    }
  else
    {// error case 
      cerr << "the name you entered doesn't match any variable" << endl; 
      return 1;
    }
}
/*!
 * @brief update the scores  
 */ 
double* agentfbfsalpha::update_score(const double& score){  
  if(TimeSerieOutput.is_open()) print();
  if (choice ==1 )
    {
      A1 = (1-fparam)*A1+score*fparam;
      A2 = A2*(1-alpha*fparam);
      A3 = A3*(1-alpha*fparam);
    }
  if (choice == 2) 
    {
      A2 = (1-fparam)*A2+score*fparam;
      A1 = (1-alpha* fparam)*A1;
      A3 = (1-alpha* fparam)*A3;
    }
  if (choice == 3) 
    {
      A3 = (1-fparam)*A3+score*fparam;
      A1 = (1-alpha* fparam)*A1;
      A2 = (1-alpha* fparam)*A2;
    }
  last_score = score ;
  return 0 ;
}

int agentfbfsalpha::HeavisideTheta(const double& x) const 
{
  if (x > 0 ) return 1.;
  else if (x == 0)  return 0;
  else return 0 ; 
} 

void agentfbfsalpha::print()
{
  TimeSerieOutput <<alpha <<"\t"
		  <<fparam <<"\t"
		  << id <<"\t"
		  <<proba <<"\t"
		  << Temp <<"\t"
		  << A1 <<"\t"
		  <<A2 <<endl;
}
#endif








