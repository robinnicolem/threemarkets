#include <boost/program_options.hpp>
#include "simulation.h"
#include "simulation.cpp"

namespace po = boost::program_options;
using namespace std;

/* Template of a simulation file */

// function which launch the simulation after having defined the agent
void ComputeTradersTimeSerie(vector<agentbase*>& tr, int num, string prefix)
  {
    
    for(int i =0; i < num; i++) tr[i]->ComputeTimeSerie(prefix+"AgentTimeSerie"
							+ to_string(i)+".dat");
  }
int main(int argc,  char **argv){
  int Nagents(200000);
  vector<agentbase*> traders;
  double d_sigma = 1.,moy_buy = 11 ,moy_sell = 10,T = .2 ,forget = .1;
  double p1(0.2),p2(.8);
  double th1(0.3),th2 =0.7;
  int iter(50000) ;//,np1(0),np2(0)  nm1p1(0),nm1p2(0),
  double alpha(1.);
  double fastfrac  = 0;
  string o_findist = "fidist.dat"; 
  string market_fich = "no";
  string prefix = "";
  int NSnapshot = -1;
  double th3 = 0.5;
  int NumTraderTimeSerie = -1;
    //double mean[2] = {0,0};
    //parametersing command line arguments
   po::options_description desc("Programm for the simulation of the multiagent with fbfs which have not fictious score, i.e. there alpha is 1");
  desc.add_options()
    ("help,h","Print help")
    ("nagents",po::value<int>(&Nagents)->default_value(20000),"Number of agents in the simulation")
    ("tradersts",po::value<int>(&NumTraderTimeSerie)->default_value(-1),"The time serie will be computed for the traderts first traders")
    ("NSnapshot",po::value<int>(&NSnapshot)->default_value(-1),"Number of steps between each snapshot of the qgents distributions, negative numbers mean no snapshot")
    ("th1",po::value<double>(&th1)->default_value(.3), "value of theta1")
    ("fastfrac",po::value<double>(&fastfrac)->default_value(0.),"fraction of agents with fparam = 1.")
    ("th2",po::value<double>(&th2)->default_value(.7),"value of theta2")
    ("th3",po::value<double>(&th3)->default_value(.5),"value of theta2")
    ("nsteps",po::value<int>(&iter)->default_value(5000),"Number of steps in the simulation")
    ("fparam",po::value<double>(&forget)->default_value(.01),"forgetting parameter of the agent")
    ("pb1",po::value<double>(&p1)->default_value(.2),"probability of buying of population 1")
    ("pb2",po::value<double>(&p2)->default_value(.8),"probability of buying of population 2")
    ("T",po::value<double>(&T)->default_value(.2),"Temperature")
    ("outdist",po::value<string>(&o_findist)->default_value("findist.dat"),"file for the distirbuation of the agents in the fimal state if set to not then no output file is created")
    ("mbuy",po::value<double>(&moy_buy)->default_value(11),"average of the bids")
    ("msell",po::value<double>(&moy_sell)->default_value(10),"average of the asks")
    ("sigma",po::value<double>(&d_sigma)->default_value(1.),"variance of the bid and asks distribution")
    ("alpha",po::value<double>(&alpha)->default_value(1.),"alpha parameter for the fictious scores")
    ("mar_ts",po::value<string>(&market_fich)->default_value("marketts.dat"),"prefix of the file where will be stored the market time serie if the name is no then no ts file is created")
    ("prefix",po::value<string>(&prefix)->default_value(""),"Prefixe to before the name of the files of the simulations"); 
  po::variables_map vm;
  try
    {
  po::store(po::parse_command_line(argc, argv, desc),
	    vm); // can throw
  if (vm.count("help"))
    {
      cout << desc << endl ;
      return 0 ;
    }
  po::notify(vm);
    }
  catch(po::error& e)
    {
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
      std::cerr << desc << std::endl;
      return 1;
    }
  // END parsing command line argument
  // Displaying the simulation parameters
  cout << "========================================" << endl
       << "Multiagents simulation for agents with  " << endl 
       << "fixed buys/sell preferences interactiong" << endl
       << "interacting with three markets" << endl 
       << "========================================" << endl ;
  cout << "pb1 = " << p1 
       << "; pb2 = " << p2 << endl;
  cout << "th1 = " << th1 
       << "; th2 = " << th2 
       << "; th3 = " << th3 << endl;
  cout << "sigma = " << d_sigma 
       << "; mbuy = " << moy_buy 
       << "; msell = " << moy_sell 
       << endl;  
  cout << "Number of itterations = " << iter << endl;
  cout << "r = " << forget << endl;
  cout << "T = " << T << endl;
  cout << "alpha = " << alpha << endl;
  cout << "fraction of fast agents = " << fastfrac << endl;
  cout << "=======================================" << endl ;
  ofstream simoutputall; 
  if (o_findist != "no")
    {
      simoutputall.open(prefix + o_findist);
    }
  // Generate market and create files which contain market time series
  string ts_m1(prefix + "id_1_" + market_fich), ts_m2(prefix + "id_2_" + market_fich), ts_m3(prefix + "id_3_" + market_fich); 
  if (market_fich =="no") {
    ts_m2= "no";
    ts_m2= "no";
    ts_m3= "no";
  }
  market m1(1,th1,ts_m1), m2(2,th2,ts_m2), m3(3,th3,ts_m3) ;
  
  // Initialisation of the traders with fixed buy/sell preferences
  traders.clear();
  int x1((Nagents*fastfrac)/2.),x2(Nagents*fastfrac),x3((Nagents*(1+fastfrac))/2.);
  for(int i=0;i<Nagents;i++){
    traders.push_back(new agentfbfsalpha);
    if (i < x1) traders[i]->init(d_sigma,1.,moy_buy,moy_sell,i,T,&p1);
    if (i >= x1 && i < x2) traders[i]->init(d_sigma,1.,moy_buy,moy_sell,i,T,&p2);
    if (i >= x2&& i < x3) traders[i]->init(d_sigma,forget,moy_buy,moy_sell,i,T,&p1);
    if (i >= x3 ) traders[i]->init(d_sigma,forget,moy_buy,moy_sell,i,T,&p2);
    traders[i]->change_parameter("alpha",alpha);
    traders[i]->init_score((double)forget); // quench disorder of the initial average score
  }
  // Add an option to get the time serie of the preferences of traders
  ComputeTradersTimeSerie(traders, NumTraderTimeSerie, prefix);
  // Run the simulation
  Simulation simu(m1,m2,m3,traders,prefix);
  simu.n_itterations(iter, NSnapshot);
  
  // Computing the final distribution of the agents
  simoutputall << "A1"  << "\t" << "A2"  << "\t"  << "A3"  << "\t" << "pb" << "\t" << "fparam" << "\t" << "T" << "\t" << "th1" << "\t" << "th2" << "\t" << "th3" << "\t" << "fastfrac"<< endl ;
  for (auto ag : traders)
    {
      simoutputall << ag->get_score()[0]<<"\t"  << ag->get_score()[1]  << "\t"<<ag->get_score()[2]<<"\t" << *((double*)ag->get_param()) << "\t"  << ag->get_forget()  << "\t" << T << "\t" << th1 << "\t" << th2  << "\t" << th3  << "\t"<< fastfrac <<  endl ;
    }
  // End Computing the final distribution of the agents
  
  return 0;
}

