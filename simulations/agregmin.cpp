

/*!
 @file agentfbfsalpha.cpp
 * @author Robin NICOLE
 * @version 1.0
 * @brief agents with fixed preferences to buy and sell, they only update their preferences toward market 1 and 2
 */

#ifndef AGENTFBFSALPHA_CPP
#include "agentfbfsalpha.h"
#define AGENTFBFSALPHA_CPP
  /*! 
    !@brief reset the parameters of the agents for a new turn
    !*/ 
void agentfbfsalpha::newturn(){
  bid = 0;
}
/*! 
 * @brief initialisation of the agent
 * @param sig : variance of the agent
 * @param forget : forgetting parameter of the agent
 * @param mb : average bid of the agent
 * @param ms : average seling price of the agent
 * @param idee : a number to identify the agent
 * @param Temp : temperatue of the agent 
 * @param probab : pointer to a double which is the probability of buying of the agent
 !*/ 
void agentfbfsalpha::init(const double& sig,const double& a_conf,const double& mb,const double& ms,const int& a_id,const double& a_expl,void* probab){
  sigma = sig ;
  if (a_expl < 0 || a_expl >1 ) cerr << "Wrong expressio of the exploration coefficient" << endl;
  expl = a_expl ; 
  conf =  a_conf ;
  mean_buy  = mb ;
  mean_sell = ms ;
  id = a_id ;
  proba = *((*double) probab);
  period = 10 ; 
  pm1 = myran.doub();  // choosing randomly a mixed strategy
  tstep = 0 ; 
}
int agentfbfsalpha::init_score(const double& var)
{
  return 0 ;
}
int agentfbfsalpha::choice_market(){
  tstep ++ ; 
  double random = myran.doub();
  if (random < pm1 ) choice = 1 ;
  else choice = 2 ;
  action = choose_action();
  return choice;
}
/*! 
  ! @brief get a random number between 0 and 1 and if it is lower than the proba of buying then buy else sell
  !*/ 
int agentfbfsalpha::choose_action(){
  double rand = myran.doub();
  if( rand < proba)
    {
      action = 0;
      return 0;
    }
  else 
    {
      action = 1;
     return 1;
    }
}
/*! 
 * @brief sigmoid(delta) is the probability to buy at market 1
  !*/ 
double agentfbfsalpha::sigmoid(const double& x,const double& Temp) const {
  if (Temp > 0 )
    return (double)1/((double)1+(double) exp(-x/Temp));
  else
    {
      if (x > 0) return 1.;
      else return 0. ;
    }
}
 /*!
  * @brief return the agent offer 
  */ 
double agentfbfsalpha::offer(){
  double output(0) ;
  if (action==0)  output = rand_gaussian(1.,mean_buy);
  if (action==1)  output = rand_gaussian(1.,mean_sell);
  bid = output;
  return output;
}

double agentfbfsalpha::rand_gaussian(const double& stdev,const double& mean) const {
  double v1,v2,s;
  do {
    v1=2.0*myran.doub()-1.;
    v2=2.0*myran.doub()-1.;
    s = v1*v1 + v2*v2;
  } while ( s >= 1.0 );

  if (s == 0.0)
    return 0.0;
  else
    return (mean+stdev*v1*sqrt(-2.0 * log(s) / s));
}

double agentfbfsalpha::get_doub(const string& name)
{
  if (name == "T") return Temp ; 
  else if (name == "cgscore") return cgscore ;
  else 
    {
      cerr << "Wrong name of variable to get" << endl ;
      return 0.; 
    }
}

int agentfbfsalpha::change_parameter(const string& name,const int& val){
  
  if (name == "period")
    {
      period = val ;
      return 0 ;
    }
  else
    {// error case 
      cerr << "the name you entered doesn't match any variable" << endl; 
      return 1;
    }
}
int agentfbfsalpha::change_parameter(const string& name,const double val){
  
  if (val == "pb")
    {
      cout << "test" << endl ;
    }
  else
    {// error case 
      cerr << "the name you entered doesn't match any variable" << endl; 
      return 1;
    }
}

/*!
 * @brief update the scores  
 */ 
double* agentfbfsalpha::update_score(const double& score){  
  if (tstep % period != 0 )
    {
      max_score = max(
}

int agentfbfsalpha::HeavisideTheta(const double& x) const 
{
  if (x > 0 ) return 1.;
  else if (x == 0)  return .5;
  else return 0 ; 
} 

void agentfbfsalpha::print()
{
  cout <<"alpha :"<< alpha<<endl;
  cout <<"choice :"<< choice<<endl;/*!< choice of the agent */
  cout <<"action :"<< action<<endl; /*!< action of the agent 0 for buy 1 for sell */
  cout <<"sigma :"<< sigma<<endl; /*!< variance of agent's bid  */
  cout <<"delta :"<< delta<<endl; /*! difference between the scores Ai at market i : A2-A1 */
  cout <<"fparam :"<< fparam<<endl; /*!< forgetting parameter of the agent */
  cout <<"bid :"<< bid<<endl; /*!< value of the bid of the agent */
  cout <<"mean_buy :"<< mean_buy<<endl;/*!<average bid          */
  cout <<"mean_sell :"<< mean_sell<<endl;/*!< average ask */
  cout <<"id :"<< id<<endl; /*!< id of the agent  */
  cout <<"success :"<< success<<endl; ; /*!< true if the last trade was successfull */
  cout <<"last_score :"<< last_score<<endl;
  cout <<"last_m :"<< last_m<<endl;/*!< last market chosen by the agent */
  cout <<"probas :"<< proba<<endl; /*!< probability of buying*/
  cout <<"Temperature :"<< Temp<<endl;
  cout <<"A1 :"<< A1<<endl;
  cout << "A2 :"<<A2<<endl;
}
#endif








